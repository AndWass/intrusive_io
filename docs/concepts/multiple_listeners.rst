Multiple listeners
==================

.. toctree::
    :hidden:

By default there is a one-to-one relationship between a ``basic_service::notify`` call
and a listener notification. This means that if one wishes to notify multiple
listeners in parallell it has to be handled at a different layer.

To support this scenario a ``multilistener_notifier`` adapter is provided. This adapter
sits between a service and its listener. Listeners will listen to the adapter instead of
to the service itself, and the adapter will be the one that listens to the service.

Motivating example
------------------

Given a ``service``, ``peripheral``, ``listener_a`` and ``listener_b``; when the listeners
are listening to the service directly, the flow of data looks like the diagram below.

.. image:: basic_notify_sequence.png

Each service notification will only cause a single listener to be notified.

By using a ``multilistener_notifier`` this can be changed to work in parallell.

.. image:: basic_multilistener_sequence.png

See ``samples/multilistener_notifier/sample_multilistener_notifier.cpp`` for a sample application
that uses a ``multilistener_notifier`` together with a ``generic_data_listener``.
