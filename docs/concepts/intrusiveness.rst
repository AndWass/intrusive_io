Intrusiveness
=============

.. toctree ::
  :hidden:

Intrusive IO is a completely intrusive library. No memory allocation
is done.

In small embedded systems, with limited RAM (in many cases measured in KB),
it is very common to statically allocate all long-term storage at the start
of the program, and maybe disallow or even disable dynamic allocation. This way the
linker can tell you when you have gone over the limit and you avoid nasty
hard-to-debug surprises at runtime. This places some limitations on both
library, and programming techniques, available.

The best way to illustrate the difference between intrusive
and non-intrusive is to consider containers. Normal containers,
such as the ones found in the C++ standard library, are almost
always non-intrusive. They will allocate **copies** of data and manage
the lifetime of those copies for you.

Intrusive containers will **not** take ownership of data, they will just **reference** it.
They often require that data derives from certain classes, or contain certain
members, so that it can be referenced and modified within the container later on.
This places the burden of lifetime management entirely on the user of
the library. In some cases this is exactly what we want though.

List example
------------

Consider the following structure

.. highlight:: c++

.. code-block:: c++

  struct person
  {
      std::string first_name;
      std::string last_name;
  };

This can easily be used with ``std::list<person>`` or
``std::vector<person>`` etc.

To use the same structure in a ``boost::intrusive::list``, or
``boost::intrusive::slist`` (double-linked or single-linked list)
the structure must be modified to **hook** it into the container.

.. code-block:: c++

  struct person: public boost::intrusive::list_base_hook<>
  {
      std::string first_name;
      std::string last_name;
  };

Now, instances can be created outside of the list, and hooked into the list:

.. code-block:: c++

  boost::intrusive::list<person> person_list;
  person[10] storage;

  for(int i=0; i<10; i++) {
      std::cin >> storage[i].first_name;
      std::cin >> storage[i].last_name;

      // Hook the person into the list
      // no memory-allocation is done,
      // no constructors are called
      person_list.push_back(storage[i]);
  }

  // Unhook all persons from the list
  // no memory deallocation is done
  // no destructors are called
  person_list.clear();
