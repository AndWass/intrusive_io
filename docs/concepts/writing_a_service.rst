Writing a service
=================

.. toctree::
    :hidden:

In this part we will cover how to write a service of our own.
This will show the core concepts of how a service works.

Service functionality
---------------------

The service will have the same functionality as ``pin_service`` which notifies its listeners whenever
a GPIO pin (General Purpose Input Output pin) has changed its value.

The GPIO pin can be of any type but would typically be an input pin, and an
interrupt would typically be what triggers the pin change notification.

The full implementation can be found in ``include/intrusive_io/pin_service.hpp``. A full skeleton example
can be found in ``samples/skeleton_service``.

Service skeleton
----------------

The base skeleton of a service is as shown below.

.. highlight:: c++

.. code-block:: c++

    #include "intrusive_io/error.hpp"
    #include "intrusive_io/execution_resource.hpp"
    #include "intrusive_io/executor_invoking_service.hpp"
    
    struct skeleton_service_listener : public intrusive_io::executor_invoking_listener<
                                           skeleton_service_listener, intrusive_io::notify_arguments<>,
                                           intrusive_io::listener_arguments<intrusive_io::status_code>>
    {
        using base_ = intrusive_io::executor_invoking_listener<
            skeleton_service_listener, intrusive_io::notify_arguments<>,
            intrusive_io::listener_arguments<intrusive_io::status_code>>;
            
        using completion_handler = typename base_::completion_handler;
        skeleton_service_listener() noexcept = default;
        skeleton_service_listener(intrusive_io::execution_resource &executor,
                                  completion_handler listener) noexcept
            : base_{executor, listener} {
        }
        void notify_hook() noexcept {
        }
        void invoke_hook() noexcept {
            this->invoke_completion_handler(intrusive_io::status_value::success);
        }
        void cancel_hook() noexcept {
            this->invoke_completion_handler(intrusive_io::status_value::operation_canceled);
        }
    };
    
    struct skeleton_service : public intrusive_io::executor_invoking_service<skeleton_service_listener>
    {
        using completion_handler = typename intrusive_io::executor_invoking_service<
            skeleton_service_listener>::completion_handler;
    };

Services requires two components: a listener which can be used to listen
for notifications from a service, and the service itself. Listener types
_must_ derive from ``basic_service_listener``, either directly or
indirectly. The most common case will be to derive from ``executor_invoking_listener`` though.

It is in the listener that data received from the ``notify_hook``
call chain typically will be stored. This is then forwarded to completion handlers from
the ``invoke_hook``. This separation of storing and forwarding is what allows us to switch from
a peripheral context to an application context. Data is stored while in peripheral context, but
is forwarded to a completion handler from within ``execution_resource::run``, which happends in
application context.

``cancel_hook`` will be called if a listener is cancelled, and should most likely forward
this to the completion handler. ``cancel_hook`` will never carry any extra data, and if
the listener derives from ``executor_invoking_listener`` the call to ``cancel_hook`` will be done
from application context.

The listener decides which notification and listener-data that the service will
use. This is done via the ``notify_arguments`` and ``listener_arguments`` template
parameters.

The second component is the service itself. The service will in most cases
derive from ``executor_invoking_service`` and only needs to know the type
of listener that the service expects.

Notify and listen with arguments
--------------------------------

The service should take a ``std::uint32_t`` and a ``bool`` as notify arguments. These
shall also be forwarded as listener arguments, but to support cancellation a third argument,
a ``status_code`` will be forwarded to listeners as well. This allows us to handle cancellations
from within a completion handler. All library services will send a ``status_code`` as the first argument
to the completion handler.

To do this we only need to change the ``skeleton_service_listener`` type.

.. code-block:: c++

    struct skeleton_service_listener
        : public intrusive_io::executor_invoking_listener<skeleton_service_listener,
                                                          intrusive_io::notify_arguments<std::uint32_t, bool>,
                                                          intrusive_io::listener_arguments<intrusive_io::status_code,
                                                            std::uint32_t, bool>>
    {
        using base_ = intrusive_io::executor_invoking_listener<skeleton_service_listener,
                                                               intrusive_io::notify_arguments<std::uint32_t, bool>,
                                                               intrusive_io::listener_arguments<intrusive_io::status_code,
                                                            std::uint32_t, bool>>;
        using completion_handler = typename base_::completion_handler;
    
        skeleton_service_listener(intrusive_io::execution_resource &executor,
                                  completion_handler listener) noexcept
            : base_{executor, listener} {
        }
        void notify_hook(std::uint32_t pin, bool value) noexcept {
            // Store the data for forwarding to completion handler
            // from within invoke_hook
            pin_ = pin;
            value_ = value;
        }
        void invoke_hook() noexcept {
            // Send a success value and the previously stored values to the completion handler.
            this->invoke_completion_handler(intrusive_io::status_value::success,
                pin_, value_);
        }
        void cancel_hook() noexcept {
            // Make sure to invoke the completion handler with a canceled status.
            this->invoke_completion_handler(intrusive_io::status_value::operation_canceled,
                0, false);
        }

    private:
        bool value_;
        std::uint32_t pin_;
    };

Adding some helpers
-------------------

To make the service have a nicer user-interface
it is good to add two helpers: one to async "subscribe"
and one to create a new instance of ``skeleton_service_listener``.

.. code-block::

    void async_wait_pin_event(skeleton_service_listener &listener) noexcept {
        this->async_listen_one(listener);
    }

    skeleton_service_listener
    make_listener(intrusive_io::execution_resource &executor,
                  completion_handler listener) noexcept {
        return skeleton_service_listener(executor, listener);
    }

Final notes
-----------

This is a very basic service but shows the very essence of how a service
is written. More advanced services will store data in the service itself
and other tricks.
