add_executable(testrunner main.cpp
    tests_basic_service.cpp
    tests_data_buffer_service.cpp
    tests_pin_service.cpp
    tests_generic_data_service.cpp
    tests_producer_consumer_service.cpp
    tests_commit_buffer.cpp
    tests_multilistener_notifier.cpp
)

target_include_directories(testrunner PRIVATE ${CMAKE_CURRENT_LIST_DIR}/include)
target_link_libraries(testrunner intrusive_io)

add_test(NAME unittests COMMAND testrunner)