#include <catch2/catch.hpp>

#include "intrusive_io/error.hpp"
#include "intrusive_io/executor_invoking_service.hpp"
#include "intrusive_io/multilistener_notifier.hpp"

struct skeleton_service_listener : public intrusive_io::executor_invoking_listener<
                                       skeleton_service_listener, intrusive_io::notify_arguments<>,
                                       intrusive_io::listener_arguments<intrusive_io::status_code>>
{
    using base_ = intrusive_io::executor_invoking_listener<
        skeleton_service_listener, intrusive_io::notify_arguments<>,
        intrusive_io::listener_arguments<intrusive_io::status_code>>;

    using completion_handler = typename base_::completion_handler;
    skeleton_service_listener() noexcept = default;
    skeleton_service_listener(intrusive_io::execution_resource &executor,
                              completion_handler listener) noexcept
        : base_{executor, listener} {
    }
    void notify_hook() noexcept {
    }
    void invoke_hook() noexcept {
        this->invoke_completion_handler(intrusive_io::status_value::success);
    }
    void cancel_hook() noexcept {
        this->invoke_completion_handler(intrusive_io::status_value::operation_canceled);
    }
};

struct skeleton_service : public intrusive_io::executor_invoking_service<skeleton_service_listener>
{
    using completion_handler = typename intrusive_io::executor_invoking_service<
        skeleton_service_listener>::completion_handler;
};

TEST_CASE("multilistener_notifier: will notify single listener", "[multilistener_notifier]") {
    intrusive_io::execution_resource executor;
    intrusive_io::multilistener_notifier<skeleton_service_listener> multilistener_notifier;
    skeleton_service service;
    bool handler_called = false;
    auto listener_handler = [&](intrusive_io::status_code sc) { handler_called = true; };
    skeleton_service_listener my_listener(executor, intrusive_io::wrap_functor(listener_handler));
    multilistener_notifier.set_default_listen_function(service);
    multilistener_notifier.async_listen_one(my_listener);
    service.notify();
    REQUIRE_FALSE(handler_called);
    executor.try_run();
    REQUIRE(handler_called);
}

TEST_CASE("multilistener_notifier: will notify multiple listener", "[multilistener_notifier]") {
    intrusive_io::execution_resource executor;
    intrusive_io::multilistener_notifier<skeleton_service_listener> multilistener_notifier;
    skeleton_service service;
    bool handler_called = false;
    auto listener_handler = [&](intrusive_io::status_code sc) { handler_called = true; };
    bool handler2_called = false;
    auto listener2_handler = [&](intrusive_io::status_code sc) { handler2_called = true; };
    skeleton_service_listener my_listener(executor, intrusive_io::wrap_functor(listener_handler));
    skeleton_service_listener my_listener2(executor, intrusive_io::wrap_functor(listener2_handler));
    multilistener_notifier.set_default_listen_function(service);
    multilistener_notifier.async_listen_one(my_listener);
    multilistener_notifier.async_listen_one(my_listener2);
    service.notify();
    REQUIRE_FALSE(handler_called);
    REQUIRE_FALSE(handler2_called);
    executor.try_run();
    REQUIRE(handler_called);
    REQUIRE(handler2_called);
}

TEST_CASE("multilistener_notifier: listeners are removed after notified",
          "[multilistener_notifier]") {
    intrusive_io::execution_resource executor;
    intrusive_io::multilistener_notifier<skeleton_service_listener> multilistener_notifier;
    skeleton_service service;
    bool handler_called = false;
    auto listener_handler = [&](intrusive_io::status_code sc) { handler_called = true; };
    bool handler2_called = false;
    auto listener2_handler = [&](intrusive_io::status_code sc) { handler2_called = true; };
    skeleton_service_listener my_listener(executor, intrusive_io::wrap_functor(listener_handler));
    skeleton_service_listener my_listener2(executor, intrusive_io::wrap_functor(listener2_handler));
    multilistener_notifier.set_default_listen_function(service);
    multilistener_notifier.async_listen_one(my_listener);
    multilistener_notifier.async_listen_one(my_listener2);
    service.notify();
    REQUIRE_FALSE(handler_called);
    REQUIRE_FALSE(handler2_called);
    executor.try_run();
    REQUIRE(handler_called);
    REQUIRE(handler2_called);
    handler_called = false;
    handler2_called = false;
    service.notify();
    executor.try_run();
    REQUIRE_FALSE(handler_called);
    REQUIRE_FALSE(handler2_called);
}

TEST_CASE("multilistener_notifier: cancelled listener is notified with status cancelled",
          "[multilistener_notifier]") {
    intrusive_io::execution_resource executor;
    intrusive_io::multilistener_notifier<skeleton_service_listener> multilistener_notifier;
    skeleton_service service;
    intrusive_io::status_code handler_status_code = intrusive_io::status_value::unknown;
    auto listener_handler = [&](intrusive_io::status_code sc) { handler_status_code = sc; };
    intrusive_io::status_code handler2_status_code = intrusive_io::status_value::unknown;
    auto listener2_handler = [&](intrusive_io::status_code sc) { handler2_status_code = sc; };
    skeleton_service_listener my_listener(executor, intrusive_io::wrap_functor(listener_handler));
    skeleton_service_listener my_listener2(executor, intrusive_io::wrap_functor(listener2_handler));
    multilistener_notifier.set_default_listen_function(service);
    multilistener_notifier.async_listen_one(my_listener);
    multilistener_notifier.async_listen_one(my_listener2);
    multilistener_notifier.cancel(my_listener);
    service.notify();
    REQUIRE(handler_status_code == intrusive_io::status_value::unknown);
    REQUIRE(handler2_status_code == intrusive_io::status_value::unknown);
    executor.try_run();
    REQUIRE(handler_status_code == intrusive_io::status_value::operation_canceled);
    REQUIRE(handler2_status_code == intrusive_io::status_value::success);
}